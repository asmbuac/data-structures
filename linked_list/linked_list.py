# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.


class LinkedListNode:
    def __init__(self, value=None, link=None):
        self.value = value
        self.link = link

    def __str__(self):
        return f"Node with value: {self.value}"


class LinkedList:
    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail
        self._length = 0

    @property
    def length(self):
        return self._length

    def insert(self, v, i=None):
        node = LinkedListNode(v)

        # if list is empty
        if self.head == None:
            # insert first node
            self.head = node
            self.tail = node
        # if index is specified and not at the end of the list
        elif i != None and i < self._length:
            # if index is equal to the first index in the list
            if i == 0:
                # insert before the head
                node.link = self.head
                self.head = node
            else:  # if between two nodes (ie, not at the start or end)
                # insert between the two nodes
                old_node = self.traverse(i - 1)
                node.link = old_node.link
                old_node.link = node
        else:  # if i == self._length (ie, i is equal to the end of the list)
            # insert node at the tail
            self.tail.link = node
            self.tail = node

        self._length += 1

    def get(self, val, i=None):
        pass

    def remove(self, val, i=None):
        pass
